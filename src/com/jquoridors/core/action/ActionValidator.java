package com.jquoridors.core.action;

import com.jquoridors.core.Game;
import com.jquoridors.core.Player;

public interface ActionValidator<T extends Game> {

    T getGame();

    Boolean isValid(Player player, Action action);
}
