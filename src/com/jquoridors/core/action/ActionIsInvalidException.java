package com.jquoridors.core.action;

import com.jquoridors.core.Player;

public final class ActionIsInvalidException extends Exception {

    private Player player;
    private Action action;

    public ActionIsInvalidException(Player player, Action action) {
        this.player = player;
        this.action = action;
    }

    @Override
    public String getMessage() {
        return String.format("%s from %s is invalid.", action, player);
    }
}
