package com.jquoridors.core.action.move;


import com.jquoridors.core.board.Position;
import com.jquoridors.core.action.Action;

public abstract class AbstractMove implements Action {

    protected Position position;

    public AbstractMove(Position position) {
//        if (position == null) {
//            throw new Exception();
//        }
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }
}
