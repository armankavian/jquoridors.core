package com.jquoridors.core.board;

public enum BoardItemType {
    CELL,
    PAWN,
    WALL,
    FLAG
}
