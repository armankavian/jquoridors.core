package com.jquoridors.core.board;

public class Wall {

    FenceDirection direction = FenceDirection.NO_DIRECTION;

    public FenceDirection getDirection() {
        return direction;
    }
}
